# Developer Guide for @flownet/lib-render-templates-dir

## Overview

The `@flownet/lib-render-templates-dir` library is a utility designed to streamline the rendering of Nunjucks templates from a specified directory. It offers a simple interface to render templates and optionally copies non-template files to an output directory. This can be particularly useful for generating static websites, configuration files, or any batch processing of templated content.

## Installation

To use the `@flownet/lib-render-templates-dir` library, you can install it via npm or yarn:

```sh
# Using npm
npm install @flownet/lib-render-templates-dir

# Using yarn
yarn add @flownet/lib-render-templates-dir
```

## Usage

The primary function provided by the library is `index`, which takes an options object to specify the input directory, output directory, and rendering context among other parameters. Below is a practical example on how to use the library to render templates.

### Example

```javascript
import renderTemplates from '@flownet/lib-render-templates-dir';

async function renderMyTemplates() {
  try {
    const result = await renderTemplates({
      dir: './templates',           // Directory containing the Nunjucks templates
      pattern: '**/*.njk',          // Pattern to match template files
      outDir: './dist',             // Output directory
      context: { title: 'Hello World' },  // Context for rendering the templates
      copyUnmatchedAlso: true,      // Option to copy files not matching the pattern
      overwriteExisting: true,      // Option to overwrite existing files in the output directory
      ignore: ['**/ignore/**']      // Pattern of files to ignore
    });

    console.log('Rendered files:', result.files);
    console.log('Unmatched files:', result.unmatched);
  } catch (error) {
    console.error('Error rendering templates:', error);
  }
}

renderMyTemplates();
```

## Functionality Explained

- **`dir`**: The directory containing your Nunjucks templates.
- **`pattern`**: A file pattern to identify which files in the directory are templates. Defaults to `**/*.njk`.
- **`outDir`**: The directory where rendered files will be output.
- **`context`**: An object containing variables to be used during template rendering.
- **`copyUnmatchedAlso`/`unmatched`**: If true, files not matching the template pattern are also copied to the output directory. The `unmatched` option is preferred; `copyUnmatchedAlso` is deprecated.
- **`overwriteExisting`**: When set to true, any existing files at the destination will be overwritten by newly rendered or copied files.
- **`ignore`**: A pattern or array of patterns specifying which files to ignore in the source directory.

## Acknowledgement

This library utilizes `nunjucks` for template rendering and `@fnet/list-files` for file pattern matching. These utilities help simplify file handling and rendering processes in the library.