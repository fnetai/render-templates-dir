import nunjucks from "nunjucks";
import fs from "node:fs";
import path from "node:path";

import fnetListFiles from "@fnet/list-files";

export default async ({
  dir,
  pattern = "**/*.njk",
  outDir,
  context = {},
  // DEPRECATED: use unmatched instead
  copyUnmatchedAlso = false,
  overwriteExisting = false,
  ignore,
  unmatched
} = {}) => {


  if (typeof unmatched !== "undefined") {
    copyUnmatchedAlso = unmatched;
  }

  // Check if templates directory exists
  if (!fs.existsSync(dir)) throw new Error(`Templates directory does not exist: ${dir}`);

  // Check if output directory exists
  if (!fs.existsSync(outDir))
    fs.mkdirSync(outDir, { recursive: true });

  const nunjucksEnv = nunjucks.configure(dir, { watch: false });

  // List all template files
  const templateFiles = await fnetListFiles({
    dir: dir,
    pattern: pattern,
    dot: true,
    ignore: ignore,
  });

  const renderedFiles = [];

  // Render all templates
  for (let templateFile of templateFiles) {
    const compiled = nunjucks.compile(
      fs.readFileSync(path.resolve(dir, templateFile), "utf8"),
      nunjucksEnv
    );
    const rendered = compiled.render(context);

    const templateFileOut = templateFile.replace(/\.njk$/, "");
    const templateFileOutPath = path.resolve(outDir, templateFileOut);

    // Check if the file already exists and if we should overwrite it
    if (!overwriteExisting && fs.existsSync(templateFileOutPath)) {
      continue;
    }

    fs.mkdirSync(path.dirname(templateFileOutPath), { recursive: true });
    fs.writeFileSync(templateFileOutPath, rendered);

    renderedFiles.push({ from: templateFile, to: templateFileOut });
  }


  const unmatchedFiles = [];
  // If copyNonMatching is true, copy files that don't match the pattern directly to outDir
  if (copyUnmatchedAlso) {
    const allFiles = await fnetListFiles({
      dir: dir,
      pattern: "**/*",
      dot: true,
      ignore
    });

    for (let file of allFiles) {
      if (!templateFiles.includes(file)) {
        const sourcePath = path.resolve(dir, file);
        const destPath = path.resolve(outDir, file);

        // Check if the file already exists and if we should overwrite it
        if (!overwriteExisting && fs.existsSync(destPath)) {
          continue;
        }

        fs.mkdirSync(path.dirname(destPath), { recursive: true });

        unmatchedFiles.push({ from: file, to: file });

        // Check if the source is a directory or a file
        if (fs.statSync(sourcePath).isDirectory()) {
          fs.mkdirSync(destPath, { recursive: true });
        } else {
          fs.copyFileSync(sourcePath, destPath);
        }
      }
    }
  }

  return {
    files: renderedFiles,
    unmatched: unmatchedFiles,
  }
}