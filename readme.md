# @flownet/lib-render-templates-dir

## Introduction

The `@flownet/lib-render-templates-dir` is a utility designed for processing and rendering template files. This tool focuses on transforming Nunjucks templates into static files based on a given context, which can then be saved to a specified output directory. The utility is straightforward, offering a structured way to manage and render templates in large directory structures.

## How It Works

This utility works by scanning a designated directory for Nunjucks template files, specified by a pattern. It then compiles these templates within the context provided by the user, and writes the rendered output to a chosen output directory. Additionally, the utility can also copy files that do not match the template pattern to the output directory, if configured to do so.

## Key Features

- **Template Rendering**: Compiles and renders Nunjucks template files using a specified context.
- **File Management**: Supports listing and processing files in directory structures.
- **Configurable Output**: Allows specifying an output directory and customizing whether to overwrite existing files.
- **Unmatched File Handling**: Provides an option to copy files that do not match the template pattern to the output directory.

## Conclusion

The `@flownet/lib-render-templates-dir` utility offers a practical way to render and manage Nunjucks templates across directories. Its straightforward approach makes it a useful tool for projects that require automatic template rendering and file management.